function readBooksPromise(time,book){

    console.log(`saya mulai membaca ${book.name}`)
    return new Promise(function (resolve,reject){
        setTimeout(function(){
            let sisaWaktu=time-book.timeSpent

            if(sisaWaktu>=0){
                console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            }else{
                console.log(`saya tidak punya waktu untuk baca ${book.name}`)
            }
        },book.timeSpent)



    })
}
module.exports=readBooksPromise


















// var isMomHappy = false;
 
// // Promise
// let willIGetNewPhone = new Promise(
//     function (resolve, reject) {
//         if (isMomHappy) {
//             let phone = {
//                 brand: 'Samsung',
//                 color: 'black'
//             };
//             resolve(phone); // fulfilled atau janji dipenuhi
//         } else {
//             let reason = new Error('mom is not happy');
//             reject(reason); // reject (ingkar)
//         }
 
//     }
// ); 

// function askMom() {
//     willIGetNewPhone
//         .then(function (fulfilled) {
//             // yay, you got a new phone
//             console.log(fulfilled);
//          // output: { brand: 'Samsung', color: 'black' }
//         })
//         .catch(function (error) {
//             // oops, mom don't buy it
//             console.log(error.message);
//          // output: 'mom is not happy'
//         });
// }
 
// // Tanya Mom untuk menagih janji
// askMom() 