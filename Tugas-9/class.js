
//no 1
    //release 0
class Animal {
    
    constructor (nama){
        this.name=nama
        this._legs= 4
        this._cold_blooded= false
        
    }
    set legs(_legs) {
        this._legs = _legs
    }
    get legs() {
        return this._legs
    }
    
    set cold_blooded(_cold_blooded) {
        this._cold_blooded = this._cold_blooded
    }
    get cold_blooded() {
        return this._cold_blooded
    }


    
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

    //release 1

    class Ape extends Animal{
        constructor(_name) {
            super(_name)
            this.legs = 2
        }
        yell() {
            console.log('Auooo');
        }
    }
    class Frog extends Animal {
        constructor(_name) {
            super(_name)
        }
        jump() {
            console.log('hop hop');
        }
    }
    var sungokong = new Ape("kera sakti")
    sungokong.yell() // "Auooo"
    // console.log(sungokong._legs);
     
    var kodok = new Frog("buduk")
    kodok.jump() // "hop hop" 

    

    class Clock {
        constructor({template}) {
            this._template = template
        }
        render() {
            var date = new Date();
        
            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
        
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
        
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;
            
            var output = this._template
              .replace('h', hours)
              .replace('m', mins)
              .replace('s', secs);
        
            console.log("render ->", output);
        }
        stop() {
            clearInterval(_timer);
        }
        start() {
            this.render;
            this._timer = setInterval(() => this.render(), 1000);
        }
    }
    
    var clock = new Clock({template: 'h:m:s'});
    clock.start(); 
    