// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 4 – Function
// Oleh: Muhammad Nevin

// Soal No. 1 (Range) 
console.log("Soal No. 1 (Range)");
function range(startNum = 'a', finishNum = 'b') {
    if(startNum == 'a' || finishNum == 'b') return -1
    var inc = 1
    if (startNum > finishNum) {
        inc = -1
    }
    var Range = []
    Range.push(startNum)
    while (startNum != finishNum) {
        startNum += inc
        Range.push(startNum)
    }
    return Range
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
console.log("Soal No. 2 (Range with Step)");
function rangeWithStep(startNum = 'a', finishNum = 'b', step = 1) {
    if(startNum == 'a' || finishNum == 'b') return -1
    var inc = step
    var Range = []
    if (startNum > finishNum) {
        inc *= -1
        while (startNum >= finishNum) {
            Range.push(startNum)
            startNum += inc
        }
    }
    else {    
        while (startNum <= finishNum) {
            Range.push(startNum)
            startNum += inc
        }
    }
    return Range
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
console.log("Soal No. 3 (Sum of Range)");
function sum(startNum = 'a', finishNum = 'b', step = 1) {
    if(startNum == 'a' || finishNum == 'b') return 0
    var inc = step
    var Range = []
    var sum = 0
    if (startNum > finishNum) {
        inc *= -1
        while (startNum >= finishNum) {
            sum += startNum
            startNum += inc
        }
    }
    else {    
        while (startNum <= finishNum) {
            sum += startNum
            startNum += inc
        }
    }
    return sum
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input) {
    for(var i = 0; i < input.length; i++) {
        console.log("Nomor ID:  " + input[i][0]);
        console.log("Nama Lengkap:  " + input[i][1]);
        console.log("TTL:  " + input[i][2] + " " + input[i][3]);
        console.log("Hobi:  " + input[i][4]);
    }
}

dataHandling(input)

// Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)");
function balikKata(str) {
    var s = str.split("")
    var r = s.reverse()
    var j = r.join("")
    return j
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)");

function dataHandling2(input) {
    console.log(input);
    var bulan = input[3]
    console.log(bulan);
    var b = bulan.split('/')
    console.log(b);
    console.log(b.join('-'));
    console.log(input[1]);
}

var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 